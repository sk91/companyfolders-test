# DB
Build database (MySQL) to store this data:

#### Products
Each product may have different sets of Options.
For example - 'Color' and 'Speed' for product 'Car' and 'Weight' and 'Size' for product 'Bed'
Only Name and price are common options for all products

#### Product Groups
Products can be grouped by sets of options - Product Groups. All products in one group will have the same set of options
For example - 'Color' and 'Speed' for product 'Car' and 'Bike'. Products may only belong to one group.

#### Categories
Product Group can be added to one or multiple Categories.  No subcategories need.
Example product group 'Sports Cars' can be added to ‘Racing’, ‘Luxury’

# Coding
it's preferably to use Phalcon, but if you think it will greatly increase time  to finish this task, than you can switch it to any framework you like

#### Create models
Create proper models to reflect all data and relations.

#### Create search page
Create a page that allow user to filter products by Categories and Option values. Page will have left sidebar and content area. Sidebar should have search filters and the content area should show the list of matching available products.
The first section of the left sidebar is list of Categories and allow each category to be selected with a checkbox.
Below Categories show list of Options with values.

Each Option value and Category has counter (number) of currently available products. This number may change depending on filter selections.
Each value can be selected with checkbox

Values inside one option are applied by OR. Values of different options are applied by AND. Categories also applied by AND.

