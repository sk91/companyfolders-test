<?php

class OptionsController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('options', Options::find());
    }

    public function createAction() {
        $this->view->disable();
        try {
            if (!isset($_POST["option_name"])) {
                throw new InvalidArgumentException("No option name");
            }
            $option = new Options();
            $option->setName($_POST["option_name"]);
            $option->setType($_POST["option_type"]);
            if ($option->create() === false) {
                $msgs = $option->getMessages();
                throw new Exception(implode("<br/>", $msgs));
            }
            $this->flash->success("option created successfully");
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
        $this->response->redirect("/options", true);
    }

    public function deleteAction() {
        $id = intval($this->dispatcher->getParam("id"), 10);
        try {
            $option = Options::findFirst($id);
            if (!$option) {
                throw new Exception("Category does not exist");
            }

            if ($option->delete() === false) {
                $msgs = $option->getMessages();
                throw new Exception(implode("<br/>", $msgs));
            }
            $this->flash->success("Category deleted successfully");
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
        $this->response->redirect("/options", true);
    }

}
