<?php

class ProductGroupsController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('categories', Categories::find());
        $this->view->setVar('options', Options::find());
        $this->view->setVar('product_groups', ProductGroups::find());
    }

    public function createAction() {
        $this->view->disable();
        try {
            if (!isset($_POST["product_group_name"])) {
                throw new InvalidArgumentException("No product_group name");
            }

            $product_group = new ProductGroups();
            $product_group->setName($_POST["product_group_name"]);
            $categories = [];
            if (isset($_POST["product_group_categories"])) {
                $categories = $_POST["product_group_categories"];
            }

            $categories = array_map(function ($cat) {
                $rel              = new ProductGroupCategory();
                $rel->category_id = intval($cat);
                return $rel;
            }, $categories);
            $product_group->productGroupCategory = $categories;
            $options                             = [];
            if (isset($_POST["product_group_options"])) {
                $options = $_POST["product_group_options"];
            }

            $options = array_map(function ($opt) {
                $rel            = new ProductGroupOptions();
                $rel->option_id = intval($opt);
                return $rel;
            }, $options);
            $product_group->ProductGroupOptions = $options;
            if ($product_group->create() === false) {
                $msgs = $product_group->getMessages();
                throw new Exception(implode("<br/>", $msgs));
            }
            $this->flash->success("product_group created successfully");
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
        $this->response->redirect("/product_groups", true);
    }

    public function deleteAction() {
        $id = intval($this->dispatcher->getParam("id"), 10);
        try {
            $product_group = ProductGroups::findFirst($id);
            if (!$product_group) {
                throw new Exception("Category does not exist");
            }

            if ($product_group->delete() === false) {
                $msgs = $product_group->getMessages();
                throw new Exception(implode("<br/>", $msgs));
            }
            $this->flash->success("Category deleted successfully");
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
        $this->response->redirect("/product_groups", true);
    }

}
