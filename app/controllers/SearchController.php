<?php

class SearchController extends ControllerBase {

    public function indexAction() {
        $selected_cat = $this->request->getQuery("categories");
        if (!$selected_cat){
            $selected_cat = [];
        }
        $selected_cat = array_map(function($cat){ return intval($cat); }, $selected_cat);
        $selected_opt = $this->request->getQuery("options");
        if (!$selected_opt){
            $selected_opt = [];
        }
        $result = Products::search($selected_cat, $selected_opt);
        $filters = [
            "cats"=>Categories::findAsFilters($selected_cat, $result),
            "opts"=>Options::findAsFilters($selected_opt, $result)
        ];
        foreach($result["prods"] as $pd){
            $pg_cats = $result["pg_cats"][$pd->product_group];
            foreach($pg_cats as $pgcat){
                $filters["cats"][$pgcat->category_id]["counter"]++;
            }
            $prd_vals = $result["prd_vals"][$pd->product_id];
            foreach($prd_vals as $prdval){
                $filters["opts"][$prdval->option_id]["counter"]++;
                $filters["opts"][$prdval->option_id]["values"][$prdval->value]["counter"]++;
            }
        }
        $this->view->setVar('result', $result);
        $this->view->setVar('filters', $filters);
    }

}
