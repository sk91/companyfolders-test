<?php

use InvalidArgumentException;

class CategoriesController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('categories', Categories::find());
    }

    public function createAction() {
        $this->view->disable();
        try {
            if (!isset($_POST["category_name"])) {
                throw new InvalidArgumentException("No category name");
            }

            $category = new Categories();
            $category->setName($_POST["category_name"]);
            if ($category->create() === false) {
                $msgs = $category->getMessages();
                throw new Exception(implode("<br/>", $msgs));
            }
            $this->flash->success("category created successfully");
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
        $this->response->redirect("/categories", true);
    }

    public function deleteAction() {
        $id = intval($this->dispatcher->getParam("id"), 10);
        try {
            $category = Categories::findFirst($id);
            if (!$category) {
                throw new Exception("Category does not exist");
            }

            if ($category->delete() === false) {
                $msgs = $category->getMessages();
                throw new Exception(implode("<br/>", $msgs));
            }
            $this->flash->success("Category deleted successfully");
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
        $this->response->redirect("/categories", true);
    }
}
