<?php

class ProductsController extends ControllerBase {

    public function indexAction() {
        $this->view->setVar('product_groups', ProductGroups::find());
        $this->view->setVar('products', Products::find());
    }

    public function createAction() {
        $this->view->disable();
        try {
            if (!isset($_POST["product_name"])) {
                throw new InvalidArgumentException("No product_ name");
            }
            if (!isset($_POST["product_price"])) {
                throw new InvalidArgumentException("No product price");
            }
            if (!isset($_POST["product_group"])) {
                throw new InvalidArgumentException("No product_group");
            }
            $product = new Products();
            $product->setName($_POST["product_name"]);
            $product->setPrice($_POST["product_price"]);
            $product->product_group = (int) $_POST["product_group"];
            if ($product->create() === false) {
                $msgs = $product->getMessages();
                throw new Exception(implode("<br/>", $msgs));
            }
            $this->flash->success("product created successfully");
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
        $this->response->redirect("/products", true);
    }

    public function deleteAction() {
        $id = intval($this->dispatcher->getParam("id"), 10);
        try {
            $product = Products::findFirst($id);
            if (!$product) {
                throw new Exception("Category does not exist");
            }

            if ($product->delete() === false) {
                $msgs = $product->getMessages();
                throw new Exception(implode("<br/>", $msgs));
            }
            $this->flash->success("Category deleted successfully");
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
        $this->response->redirect("/products", true);
    }

    public function editOptionsAction() {
        $id = intval($this->dispatcher->getParam("id"), 10);
        try {
            $product = Products::findFirst($id);
            if (!$product) {
                $this->flash->error("Product does not exist");
                return $this->response->redirect("/products", true);
            }
            $options      = $product->ProductGroups->options;
            $options_vals = [];
            foreach ($options as $opt) {
                $option_vals[$opt->option_id] = [
                    "opt" => $opt,
                    "val" => "",
                ];
            }
            foreach ($product->ProductOptionsValues as $opval) {
                if (isset($option_vals[$opval->option_id])) {
                    $option_vals[$opval->option_id]["val"] = $opval->value;
                }
            }
            $this->view->setVar('product', $product);
            $this->view->setVar('option_vals', $option_vals);
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
    }
    public function saveOptionsAction() {
        $this->view->disable();
        $id = intval($this->dispatcher->getParam("id"), 10);
        try {
            $product     = Products::findFirst($id);
            $options     = $product->ProductGroups->options;
            $option_vals = [];
            foreach ($options as $opt) {
                if (!isset($_POST["product_opt_" . $opt->option_id])) {
                    continue;
                }
                $opt_val                     = $_POST["product_opt_" . $opt->option_id];
                $product_opt_val             = new ProductOptionsValues();
                $product_opt_val->option_id  = $opt->option_id;
                $product_opt_val->product_id = $id;
                $product_opt_val->value      = $opt_val;
                array_push($option_vals, $product_opt_val);
            }
            $product->ProductOptionsValues = $option_vals;
            if ($product->save() !== true) {
                $msgs = $product->getMessages();
                throw new Exception(implode("<br/>", $msgs));
            }
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
        $this->response->redirect("/products/" . $id . "/options", true);
    }
}
