<?php

class Categories extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="category_id", type="integer", length=10, nullable=false)
     */
    public $category_id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=45, nullable=false)
     */
    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSchema("companyfolders");
        $this->setSource("categories");
        $this->hasMany(
            'category_id',
            'ProductGroupCategory',
            'category_id'
        );
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Categories[]|Categories|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    public static function findAsFilters($checked = []){
        $cats = self::find();
        $filters = [];
        $checked_map = [];
        foreach($checked as $cat){
            $checked_map[$cat] = true;
        }
        foreach($cats as $cat){
            $id = $cat->category_id;
            if (isset($filters[$id])){
                continue;
            }
            $filters[$id] = [
                "category_id" => $cat->category_id,
                "name" => $cat->name,
                "counter" => 0,
                "checked" => isset($checked_map[$id]) ? "checked" : ""
            ];
        }
        return $filters;
    }

    public static function fetchProductGroupCategories($product_groups = [], $map = true){
        $category = new Categories();
        $cats_where = "";
        if (count($product_groups))
        {
            $product_groups = implode(",", $product_groups);
            $cats_where = "AND product_group_category.product_group_id in ($product_groups)";
        }
        $product_group_categories = $category->getReadConnection()->query("
            SELECT product_group_category.product_group_id, categories.category_id, categories.name
            FROM categories, product_group_category
            WHERE
                categories.category_id = product_group_category.category_id
                $cats_where;
        ");
        $product_group_categories->setFetchMode(Phalcon\Db::FETCH_OBJ);
        $product_group_categories = $product_group_categories->fetchAll();
        if (!$map){
            return $product_group_categories;
        }
        $pg_cats = [];
        foreach($product_group_categories as $pc){
            $pg_id = $pc->product_group_id;
            if (!isset($pg_cats[$pg_id])){
                $pg_cats[$pg_id] = [];
            }
            array_push($pg_cats[$pg_id], $pc);
        }
        return $pg_cats;
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Categories|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'categories';
    }

    public function setName($name) {
        if (strlen($name) < 1) {
            throw new InvalidArgumentException("The name is too short");
        }

        $this->name = $name;
    }

}
