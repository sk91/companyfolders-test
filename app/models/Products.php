<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Products extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="product_id", type="integer", length=10, nullable=false)
     */
    public $product_id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=100, nullable=false)
     */
    public $name;

    /**
     *
     * @var double
     * @Column(column="price", type="double", nullable=false)
     */
    public $price;

    /**
     *
     * @var integer
     * @Column(column="product_group", type="integer", length=11, nullable=true)
     */
    public $product_group;

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSchema("companyfolders");
        $this->setSource("products");
        $this->belongsTo("product_group", "ProductGroups", "product_group_id");
        $this->hasMany("product_id", "ProductOptionsValues", "product_id");
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Products[]|Products|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Products|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    public static function search($cats = [], $opts = []) {
        $bind = [];
        $bind_id = 0;
        $sql_tables = ["products"];
        $sql_where = [];
        $cat_len = count($cats);
        $cats_str = implode(",", $cats);
        // All product groups that belong to all $cats categories
        $cats_sql = "
            (
                SELECT product_group_category.product_group_id
                FROM product_group_category
                WHERE product_group_category.category_id IN ($cats_str)
                GROUP BY product_group_category.product_group_id
                HAVING count(*)=$cat_len
            ) AS cats
        ";
        $cats_where = "products.product_group = cats.product_group_id";
        if ($cat_len>0){
            array_push($sql_tables, $cats_sql);
            array_push($sql_where, $cats_where);
        }

        $opt_sql_arr = [];
        // All products that have all option and for each option one of option values
        foreach ($opts as $optind => $opt){
            $val_binds = [];
            if (count($opt)==0){
                continue;
            }
            foreach($opt as $valind=>$val){
                $bind_id++;
                array_push($val_binds, ":opt_val_${bind_id}");
                $bind["opt_val_${bind_id}"] = $val;
            }
            $bind_id++;
            $val_binds = implode(",", $val_binds);
            $opt_sql = "(options.option_id=:opt_id_${bind_id} AND product_options_values.value IN (${val_binds}))";
            $bind["opt_id_${bind_id}"] = $optind;
            array_push($opt_sql_arr, $opt_sql);
        }
        $opt_len = count($opt_sql_arr);
        $opt_sql_arr = implode(" OR ", $opt_sql_arr);
        $opts_sql = "(
                SELECT products.product_id, options.name, options.option_id
                FROM products, options, product_options_values
                WHERE
                    products.product_id = product_options_values.product_id AND
                    options.option_id = product_options_values.option_id AND
                    (${opt_sql_arr})
                GROUP BY products.product_id
                HAVING count(*) = $opt_len
            ) AS opts
        ";
        $opts_where = "opts.product_id = products.product_id";
        if ($opt_len>0){
            array_push($sql_tables, $opts_sql);
            array_push($sql_where, $opts_where);
        }
        $sql = "SELECT products.* FROM";
        $sql .= " ".implode(",", $sql_tables);
        if (count($sql_where)){
            $sql .= " WHERE ".implode(" AND ", $sql_where);
        }
        $sql .= ";";

        $product = new Products();
        $products = $product->getReadConnection()->query($sql, $bind);
        $products->setFetchMode(Phalcon\Db::FETCH_OBJ);
        $products = $products->fetchAll();
        $product_groups = [];
        $prod_ids = [];
        foreach($products as $prod){
            $product_groups[$prod->product_group] = $prod->product_group;
            $prod_ids[$prod->product_id] = $prod->product_id;
        }
        $pg_cats = Categories::fetchProductGroupCategories($product_groups);
        $prd_vals = Options::fetchProductOptions($prod_ids);
        return [
            "prods" => $products,
            "pg_cats" => $pg_cats,
            "prd_vals" => $prd_vals
        ];
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'products';
    }

    public function setName($name) {
        if (strlen($name) < 1) {
            throw new InvalidArgumentException("The name is too short");
        }
        $this->name = $name;
    }

    public function setPrice($price) {
        $price = floatval($price);
        if ($price < 0) {
            throw new InvalidArgumentException("Price cannot be negative");
        }
        $this->price = $price;
    }

}
