<?php

class ProductGroups extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="product_grop_id", type="integer", length=10, nullable=false)
     */
    public $product_group_id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=45, nullable=false)
     */
    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSchema("companyfolders");
        $this->setSource("product_groups");
        $this->hasManyToMany(
            'product_group_id',
            'ProductGroupOptions',
            'product_group_id',
            'option_id',
            'Options',
            'option_id',
            ["alias" => 'options']
        );
        $this->hasMany(
            'product_group_id',
            'ProductGroupOptions',
            'product_group_id'
        );
        $this->hasMany(
            'product_group_id',
            'ProductGroupCategory',
            'product_group_id'
        );
        $this->hasMany(
            'product_group_id',
            'Products',
            'product_group'
        );
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductGroups[]|ProductGroups|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductGroups|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'product_groups';
    }

    public function afterSave() {
        $this->refresh();
    }

    public function setName($name) {
        if (strlen($name) < 1) {
            throw new InvalidArgumentException("The name is too short");
        }

        $this->name = $name;
    }

}
