<?php
use \Phalcon\Mvc\Model\Query\Builder;
class Options extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="option_id", type="integer", length=10, nullable=false)
     */
    public $option_id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=45, nullable=false)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(column="type", type="string", nullable=false)
     */
    public $type;

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSchema("companyfolders");
        $this->setSource("options");
        $this->hasMany(
            'option_id',
            'ProductGroupOptions',
            'option_id'
        );
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Options[]|Options|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    public static function findAsFilters($selected_opts = []){
        $builder = new Builder();
        $opt_vals = $builder
            ->addFrom("ProductOptionsValues", "vals")
            ->rightJoin("Options", null, "opts")
            ->columns(["vals.*", "opts.*"])
            ->getQuery()
            ->execute();
        $filters = [];
        foreach ($opt_vals as $opt) {
            $id = $opt->opts->option_id;
            if (!isset($filters[$id])){
                $filters[$id] = [
                    "option_id" => $id,
                    "name" => $opt->opts->name,
                    "counter" => 0,
                    "values" => []
                ];
            }
            if (!isset($filters[$id]["values"][$opt->vals->value]))
            {
                $checked = "";
                if (isset($selected_opts[$id]) && in_array($opt->vals->value, $selected_opts[$id]))
                {
                    $checked = "checked";
                }
                $filters[$id]["values"][$opt->vals->value] = [
                    "checked" => $checked,
                    "counter" => 0,
                    "option_id" => $id,
                    "value" => $opt->vals->value
                ];
            }
        }
        return $filters;
    }

    public static function fetchProductOptions($prod_ids = [], $map = true){
        $prd_vals = [];
        if (count($prod_ids)){
            $prod_ids = implode(",", $prod_ids);
            $builder = new Builder();
            $prd_vals = $builder
                ->columns(["opts.name", "opts.option_id", "vals.value", "vals.product_id"])
                ->addFrom("ProductOptionsValues", "vals")
                ->rightJoin("Options", null, "opts")
                ->where("vals.product_id in ($prod_ids)")
                ->getQuery()
                ->execute();
        }
        if (!$map){
            return $prd_vals;
        }
        $vals_map = [];
        foreach($prd_vals as $prdv){
            if(!isset($vals_map[$prdv->product_id])){
                $vals_map[$prdv["product_id"]] = [];
            }
            array_push($vals_map[$prdv["product_id"]], $prdv);
        }
        return $vals_map;
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Options|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'options';
    }

    public function setName($name) {
        if (strlen($name) < 1) {
            throw new InvalidArgumentException("The name is too short");
        }

        $this->name = $name;
    }
    public function setType($type) {
        if ($type != "text") {
            $type = "num";
        }

        $this->type = $type;
    }
}
