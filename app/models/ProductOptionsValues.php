<?php

class ProductOptionsValues extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Column(column="product_id", type="integer", length=10, nullable=true)
     */
    public $product_id;

    /**
     *
     * @var integer
     * @Column(column="option_id", type="integer", length=10, nullable=true)
     */
    public $option_id;

    /**
     *
     * @var string
     * @Column(column="value", type="string", length=45, nullable=true)
     */
    public $value;

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSchema("companyfolders");
        $this->setSource("product_options_values");
        $this->belongsTo("product_id", "Products", "product_id");
        $this->belongsTo("option_id", "Options", "option_id");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'product_options_values';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductOptionsValues[]|ProductOptionsValues|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductOptionsValues|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

}
