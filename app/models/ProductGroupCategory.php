<?php

class ProductGroupCategory extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Column(column="product_group_id", type="integer", length=10, nullable=true)
     */
    public $product_group_id;

    /**
     *
     * @var integer
     * @Column(column="category_id", type="integer", length=10, nullable=true)
     */
    public $category_id;

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSchema("companyfolders");
        $this->setSource("product_group_category");
        $this->belongsTo(
            "product_group_id",
            "ProductGroups",
            "product_group_id"
        );
        $this->belongsTo(
            "category_id",
            "Categories",
            "category_id"
        );
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductGroupCategory[]|ProductGroupCategory|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductGroupCategory|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'product_group_category';
    }

}
