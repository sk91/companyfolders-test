<?php

class ProductGroupOptions extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Column(column="product_group_id", type="integer", length=10, nullable=false)
     */
    public $product_group_id;

    /**
     *
     * @var integer
     * @Column(column="option_id", type="integer", length=10, nullable=false)
     */
    public $option_id;

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSchema("companyfolders");
        $this->setSource("product_group_options");
        $this->belongsTo(
            "product_group_id",
            "ProductGroups",
            "product_group_id"
        );
        $this->belongsTo(
            "option_id",
            "Options",
            "option_id"
        );
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'product_group_options';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductGroupOptions[]|ProductGroupOptions|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProductGroupOptions|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

}
