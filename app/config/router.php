<?php
use \Phalcon\Mvc\Router;

$router = new Router(false);
$router->setDI($di);
// Define your routes here
$router->addGet("/", [
    "controller" => "index",
    "action"     => "index",
]);

$router->addGet("/options", [
    "controller" => "options",
    "action"     => "index",
]);

$router->addPost("/options", [
    "controller" => "options",
    "action"     => "create",
]);

$router->addPost("/options/:int/delete", [
    "controller" => "options",
    "action"     => "delete",
    "id"         => 1,
]);

$router->addGet("/categories", [
    "controller" => "categories",
    "action"     => "index",
]);

$router->addPost("/categories", [
    "controller" => "categories",
    "action"     => "create",
]);

$router->addPost("/categories/:int/delete", [
    "controller" => "categories",
    "action"     => "delete",
    "id"         => 1,
]);

$router->addGet("/products", [
    "controller" => "products",
    "action"     => "index",
]);

$router->addPost("/products", [
    "controller" => "products",
    "action"     => "create",
]);

$router->addPost("/products/:int/delete", [
    "controller" => "products",
    "action"     => "delete",
    "id"         => 1,
]);

$router->addGet("/products/:int/options", [
    "controller" => "products",
    "action"     => "editOptions",
    "id"         => 1,
]);

$router->addPost("/products/:int/options", [
    "controller" => "products",
    "action"     => "saveOptions",
    "id"         => 1,
]);

$router->addGet("/product_groups", [
    "controller" => "product_groups",
    "action"     => "index",
]);

$router->addPost("/product_groups", [
    "controller" => "product_groups",
    "action"     => "create",
]);

$router->addPost("/product_groups/:int/delete", [
    "controller" => "product_groups",
    "action"     => "delete",
    "id"         => 1,
]);

$router->addGet("/search", [
    "controller" => "search",
    "action"     => "index",
]);

$router->handle();
