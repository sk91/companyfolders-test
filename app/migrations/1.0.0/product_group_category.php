<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ProductGroupCategoryMigration_100
 */
class ProductGroupCategoryMigration_100 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('product_group_category', [
                'columns' => [
                    new Column(
                        'product_group_id',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'size' => 10,
                            'first' => true
                        ]
                    ),
                    new Column(
                        'category_id',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'size' => 10,
                            'after' => 'product_group_id'
                        ]
                    )
                ],
                'indexes' => [
                    new Index('relate', ['product_group_id', 'category_id'], 'UNIQUE'),
                    new Index('fk_product_group_category_2_idx', ['category_id'], null)
                ],
                'references' => [
                    new Reference(
                        'fk_product_group_category_1',
                        [
                            'referencedTable' => 'product_groups',
                            'referencedSchema' => 'companyfolders',
                            'columns' => ['product_group_id'],
                            'referencedColumns' => ['product_group_id'],
                            'onUpdate' => 'NO ACTION',
                            'onDelete' => 'CASCADE'
                        ]
                    )
                ],
                'options' => [
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ],
            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
